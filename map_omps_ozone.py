import h5py
import glob 
from plotter_tools import *

if __name__ == '__main__':
    yr = 2018
    mn = 1
    dy = 1

    #Grabbing all NMTO3 L2 files from date provided above
    files = glob.glob('/tis/OMPS-NPP/61004/NMTO3-L2/'+str(yr)+'/'+str(mn).zfill(2)+'/'+str(dy).zfill(2)+'/*.h5')

    #Creating a map to use for plotting 
    fig, axes = make_map()

    for filename in files:

        f = h5py.File(filename,'r')

        #Reading in ozone data, setting fill values to NaN, and flattening to 1-d array for plotting
        ozone = f['/ScienceData/ColumnAmountO3'][:]
        ozone[ozone == f['/ScienceData/ColumnAmountO3'].fillvalue] = np.nan
        ozone = ozone.flatten()
    
        #Reading Lat/Lon corners and reshaping to 2-d array as expected by plotter function....(ncorner,nsample)
        lon_corner = f['/GeolocationData/LongitudeCorner'][:]
        lat_corner = f['/GeolocationData/LatitudeCorner'][:]

        lon_corner = np.swapaxes(lon_corner.reshape(len(lon_corner[:,0,0])*len(lon_corner[0,:,0]),len(lon_corner[0,0,:])),0,1)
        lat_corner = np.swapaxes(lat_corner.reshape(len(lat_corner[:,0,0])*len(lat_corner[0,:,0]),len(lat_corner[0,0,:])),0,1)

        #Plotting ozone polygons, lon_corner and lat_corner should be provided as (ncorners,nsamples) and 
        #ozone is provided as flattened 1d array
        im = plotter(fig,axes,ozone,lon_corner,lat_corner,bounds=[200,500,30],cmap=plt.cm.jet)

    #Placing colorbar on map, title, and saving the map
    cb= fig.colorbar(im,ticks=np.arange(200,550,50),fraction=0.025)
    cb.set_label('Ozone (DU)',fontsize=20)

    #Giving the map a title and saving the file
    plt.suptitle('OMPS NMTO3 Ozone '+str(yr)+'m'+str(mn).zfill(2)+str(dy).zfill(2),fontsize=25)
    plt.savefig('OMPS_NMTO3_ColumnAmountO3_'+str(yr)+'m'+str(mn).zfill(2)+str(dy).zfill(2)+'.png',bbox_inches='tight')

