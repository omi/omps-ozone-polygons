import numpy as np
from pylab import *
import cartopy.crs as ccrs
from matplotlib.collections import PolyCollection

#Creating norm/bounds variables for discrete colorbar
def make_colormap(lower,upper,step,cmap=plt.cm.jet):
    bounds = np.linspace(lower,upper,step+1)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    return norm, cmap,bounds

#Making a map with platecarree projection 
def make_map():

    fig, axes = plt.subplots(1,figsize=(10,5),subplot_kw=dict(projection=ccrs.PlateCarree()))
    axes.set_xticks(np.arange(-180,210,30))
    axes.set_yticks(np.arange(-90,110,20))
    axes.grid()
    axes.coastlines(resolution='50m')
    return fig, axes 

def plotter(fig,axes,data,lon_corns,lat_corns,bounds=[],cmap=plt.cm.jet):
    #Making colormap with provided bounds
    norm, cmap, bounds = make_colormap(bounds[0],bounds[1],bounds[2])

    #Converting lat/lon corners to polygon vertices as expected by polycollection function 
    oned_clat = np.dstack((lat_corns[0,:],lat_corns[1,:],lat_corns[2,:],lat_corns[3,:]))
    oned_clon = np.dstack((lon_corns[0,:],lon_corns[1,:],lon_corns[2,:],lon_corns[3,:]))
    verts = np.dstack((oned_clon[0,:,:],oned_clat[0,:,:]))

    #Plotting data where lon is -90 to 90, these FoVs do not have dateline crossing issues 
    inds, = np.where((~np.isnan(data))&  (np.abs(lon_corns[0,:]) < 90) )

    plot_data = data[inds]
    coll = PolyCollection(verts[inds,:,:], array=plot_data, cmap=cmap,norm=norm,linewidth=0,antialiased=False,rasterized=True)
    axes.add_collection(coll)


    #Some FoVs cross the dateline causing plotting issues so for any FoVs that could have this issue the lons are trasnformed
    #from -180 to 180 into 0 to 360 so that the dateline is in the center
    inds_neg = np.where( (verts[:,:,0] < 0))
    inds_pos = np.where( (verts[:,:,0] > 0))
    
    verts[:,:,0][inds_neg] = verts[:,:,0][inds_neg]  + 180
    verts[:,:,0][inds_pos] = verts[:,:,0][inds_pos]  - 180
 
    #Some FoVs near the pole are awkward shape crossing many degrees of longitude and wrapping across the pole. We remove these by simply 
    #excluding polygons that have a 20 degree change in longitude as FoVs should not be anywhere close to that size except for these
    #odd shaped polygons at the pole. 
    lon1 = lon_corns[0,:];lon2 = lon_corns[1,:] ;lon3 = lon_corns[2,:]; lon4 = lon_corns[3,:]
    lon_check =  np.nanmax(np.abs(np.vstack((lon1,lon2,lon3,lon4))),axis=0) - np.nanmin(np.abs(np.vstack((lon1,lon2,lon3,lon4))),axis=0)

    #Simply finding the FoVs with possible dateline issues (defined as long of -180 to -90 or 90 to 180) and the strange FoVs at the poles
    inds, = np.where((~np.isnan(data))&  (np.abs(lon_corns[0,:]) > 90) & (lon_check < 20))

    #Plotting the FoVs that are shifted due to the dateline. In order to do this with polycollection we need to provide the transform keyword as below
    plot_data = data[inds]
    coll = PolyCollection(verts[inds,:,:], array=plot_data, cmap=cmap,norm=norm,linewidth=0,antialiased=False,rasterized=True,transform=ccrs.PlateCarree(-180))
    axes.add_collection(coll)
    coll.set(array=plot_data, cmap=cmap,norm=norm)

    return coll 
 
    
