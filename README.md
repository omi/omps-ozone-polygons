# OMPS Ozone Polygons



## Getting started

This tool is used to map OMPS ozone using the lat/lon corners from OMPS. 

There is a driver called map_omps_ozone which simply reads the ozone and lat/lon corners from OMPS and prepares a map. 

Then it calls plotter from plotter_tools.py to map the OMPS ozone on the map with the polygons 

The function plotter expects the inputs (fig,axes,data,lon_corns,lat_corns,bounds=[])

where..

fig, axes are your figure and axes objects
data is a 1-d flattened array size (nsample) with the data desired to be mapped
lon_corns, lat_corns are size (4,nsample) to make the tool flexible for different satellites, products, etc
bounds is a list of size 3 [bottom_colorbar_value,top_colorbar_value,number_color_steps]
cmap is the colormap used for the plotting (something like plt.cm.jet)
